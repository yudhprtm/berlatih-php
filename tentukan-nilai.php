<?php
function tentukan_nilai($number)
{
    //  kode disini
    if ($number > 76) {
        echo "Nilai " .$number ." Sangat Baik <br>";
    } else if ($number > 67 && $number<= 76) {
        echo "Nilai " .$number . " Baik <br>";
    } else if ($number >43 && $number <=67) {
        echo "Nilai " .$number . " Cukup <br>";
    } else if ($number <=43 && $number>0) {
        echo "Nilai " .$number . " Kurang";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang