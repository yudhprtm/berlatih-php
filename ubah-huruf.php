<?php
function ubah_huruf($string){
$huruf = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
$tempat = "";

for ($a = 0; $a < strlen($string); $a++) {
    for ($i=0; $i<count($huruf); $i++) {
        if($huruf[$i] == $string[$a]){
            echo $huruf[$i].' => ' .$huruf[$i+1]. '<br>';
            $tempat .= $huruf[$i+1]; 
        }
    }
}
echo $tempat.'<br>';
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>